---
title: "Hello World"
date: 2021-11-02T08:47:04-06:00
draft: false
---

## So, this is new. 

Ok, so I have never blogged before, bare with me here. 

## Then why are you blogging now?

Long story short, its a project for high school. 

## Ok, so will this be the most boring blog ever? 

I sure hope not. I will be talking about code, self hosting, and Linux in general. I am a Linux nerd. I distro hop, but my most recent distro is NixOS, which is awesome. 
