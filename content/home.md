---
title: "Home"
date: 2021-11-02T08:52:38-06:00
url: /
draft: false
---
## Welcome to the official website of Fenix Guthrie. 
Check out some of my work: 

* [SpacebusOS](https://github.com/FedX-sudo/SpaceBusOS)

## Some exciting blog posts. 

* [I am Migrating off GitHub](./blog/Migration_announcment)
* [Hello World!](./blog/hello_world)

