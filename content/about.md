+++
title = "About"
description = "Fenix Guthrie"
date = "2019-02-28"
aliases = ["about-us", "about-fenix", "contact"]
author = "Fenix Guthrie"
+++

Hi, my name is Fenix Guthrie. I am an amatrue Linux developer, and high school student (humble brag). I am a Linux nerd, and feel free to nerd out with me. I have only been using Linux for a year now, and if you don't know, my prefred distro is now NixOS, although I do distro hop. 

Contact: 
* @fedx:matrix.org
* fedx-sudo@pm.me

