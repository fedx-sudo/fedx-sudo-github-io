---
title: "Migration Announcment"
date: 2021-11-04T12:36:06-06:00
draft: false
---


## I am *mostly* leaving GitHub. 

This is not entirly because of the new CEO at GitHub, but it did spur me onward. I simply don't like the idea of keeping my open source code on a cloused source service, and I am not a fan of GitHub's politics, such as their Copilot AI, and the simple fact they are owned by Microsoft. I have decided, I will be keeping all my code in a public Gitea instance hosted on Linode.  Bear with me durring this transition, as I am very (very) new to hosting, and I in fact don't own a domain. I will be posting updates as I get further aloung in this proecess. 

## Wait, what does "mostly" mean?

Ok, so I unfortunatly cannot esscape the clutches of Microsoft, and I will need to keep a presence on GitHub to contribute to Nixpkgs, and I will most likley keep public mirros of some of my repos hosted on GitHub, but I am undecided at the moment. 

## Ok, but what is Gitea? 

This is the fun part of the post! Gitea is a self hosted implimentation of a git repo hosting service like GitHub or GitLab. It is an awesome open source project written in Go using PostgresSQL for the backend. I am currently working on my personal self-hosted implimentation of Gitea where I keep my enmergency stash of repos in case the internet goes down and I feel like recompiling the Linux kernel. Until I feel confident hosting Gitea I will continue working my main public facing repos, namly SpacBusOS, on GitHub. 
